"use strict"

let loginFormSelector = '#sign_in';
let signupFormSelector = '#sign_up';
let SIGNUPGROUP = '';

function popUpMessage(message, type='warning') {
    console.log('popup', message, type);
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });
    Toast.fire({
        icon: type,
        title: message
    });
}


var signUpValidate = $(signupFormSelector).validate({
    rules: {
        "name": {
          required: true,
          minlength: 4
        },
        "email": {
            required: true,
            email: true
        },
        "login": {
            required: true,
            digits: true,
            maxlength: 10
        },
        "accept":{
            required: true
        }
    },
    messages: {
        "name": {
            required: "Please enter the User Name",
            minlength: "Please enter Valid User Name"
        },
        "login": {
            required: "Please enter phone number",
            maxlength: "Please enter valid phone number"
        },
        "accept":{
            required: "Please accept Term and condition"
        }
    }
});

class authentication {
    constructor() {
    }

    getLoginData() {
       let token = localStorage.getItem('authToken');
       let data = JSON.parse(localStorage.getItem('data'));
       let sessionID = localStorage.getItem('sessionID');
       return {token: token, data: data, sessionID: sessionID}
    }

    setLoginData(data) {
        localStorage.setItem('authToken', data.authToken);
        localStorage.setItem('data', JSON.stringify(data.data));
        localStorage.setItem('sessionID', data.sessionID);
        return true;
    }

    removeLoginData() {
        localStorage.removeItem('authToken');
        localStorage.removeItem('data');
        localStorage.removeItem('sessionId');
        http.deleteCookies();
    }

    clearLocalstrorage() {
        localStorage.clear();
        http.deleteCookies();
    }

    serializeObject(formData) {
        var o = {meta:{}};
        var a = formData;
        $.each(a, function() {            
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
      //  o['groupinfo'] = true;
        return o;
    }
    
    isLogin() {
        let userLoginData = this.getLoginData();
        if(sitePath=='/login') {
            if(userLoginData.sessionID != undefined || userLoginData.sessionID != null){
                window.location = dashbardPath;
            }
        } else {
            if(userLoginData.sessionID == undefined || userLoginData.sessionID == null){
                window.location = loginPath;
            }
        }
    }
    loginFormRemember(){
        var loginFormData=http.getLocalstroage('login-form');
        this.populate(loginFormSelector, loginFormData);
    }

     populate(frm, data) {
         data = JSON.parse(data);
         if(data){ 
            $.each(data, function(key, value) {  
                var ctrl = $('[name='+key+']', frm);  
                switch(ctrl.prop("type")) { 
                    case "radio": case "checkbox":   
                        ctrl.each(function() {
                            if($(this).attr('value') == value) $(this).attr("checked",value);
                        });   
                        break;  
                    default:
                        ctrl.val(value); 
                }  
            });
        }
    }
}

let loginAuth = new authentication();

$(signupFormSelector).submit(function(event) {
    event.preventDefault();
    $(".bgBlur").css("filter", "blur(2px)");
    $('#loader').removeAttr('hidden');
   
    if(signUpValidate.form()) {
        let formData = $(signupFormSelector).serializeArray();    
        let serializeObjectForm = loginAuth.serializeObject(formData); 

        let urlData =  http.parseURLParams(mylocation);
        urlData['mx_City'] = serializeObjectForm.city;
        let meta = {'meta':{'phone':'+91'+serializeObjectForm.login,
                        'tocAgree':serializeObjectForm.tocAgree,
                        'lsData': urlData
                    }
                };
        delete serializeObjectForm.countryCode;
        delete serializeObjectForm.tocAgree;
        serializeObjectForm['autologin']=true;

        let rawdata = JSON.stringify({...serializeObjectForm,...meta});

        http.post({url:apiUrl+'auth/register', data:rawdata, contentType:'application/json'},(data,textStatus)=>{
          
            if(textStatus == '402' || textStatus ==  '403'){
                //$('p.signupMessage').html(data.responseJSON.message); 
                $('#loader').attr("hidden",true);
                $("#sign_up").trigger("reset"); 
                $(".bgBlur").css("filter", "blur(0px)");
                popUpMessage(data.message,'warning');
               
                return;
            }
           
            popUpMessage('Register Success', 'success');
            $("#sign_up").trigger("reset"); 
            loginAuth.setLoginData(data);
            if(serializeObjectForm.group=='satcp'){
                window.location = satDeliver;
            } else if(serializeObjectForm.group=='greoc'){

            } else if(serializeObjectForm.group=='gmatoc'){

            }
        });
    } else {
        console.log('could not be validated');
        $('#loader').attr("hidden",true);
        $("#sign_up").trigger("reset"); 
        $(".bgBlur").css("filter", "blur(0px)");
        popUpMessage('Invalid Register', 'error');
    }
});

//logout
$('.logout').on('click', function(){
    http.setCookie('screen',null,0);
    loginAuth.clearLocalstrorage();
    window.location = loginPath;
});

$("#mod").on('click',()=>{
    $("#sign_up").trigger('reset');
});
$("#mod2").on('click',()=>{
    $("#sign_up").trigger('reset');
    $('.selectGroup').val('satcp');
    SIGNUPGROUP = 'satcp';
});

$("#mod3").on('click',()=>{
    $("#sign_up").trigger('reset');
    $('.selectGroup').val('greoc');
    SIGNUPGROUP = 'greoc';
});

$("#mod4").on('click',()=>{
    $("#sign_up").trigger('reset');
    $('.selectGroup').val('gmatoc');
    SIGNUPGROUP = 'gmatoc';
});

window.onload = function(){

   $('#google-btn').on('click', function() {
        $('.showinput').show();
       $("#sign_up").hide();
    });
    $('.backBtn').on('click', function() {
        $('.showinput').hide();
        $("#sign_up").show();
    });
    //facebook btn
    $('#facebook-btn').on('click', function() {
        $('.showfbinput').show();
        $("#sign_up").hide();
    });
    $('.backBtnFacebook').on('click', function() {
        $('.showfbinput').hide();
        $("#sign_up").show();
    });
    
    
    gapi.load('auth2', function() {
    
        var auth2 = gapi.auth2.init({
          //client_id: '281321916149-om0jbnpuoc6n2phtsm5kq7ut0vi20r96.apps.googleusercontent.com',
          client_id: '712846612819-2362i83slqo78mv6g6baof26j7pgka6d.apps.googleusercontent.com',
          cookiepolicy: 'single_host_origin',
          scope: 'profile email'
        });
    
      document.getElementById('google-button').addEventListener('click', function() {
          var googlePhone = $('#phoneGoogle').val();
          if(googlePhone == ''){
              popUpMessage('Phone number is mandatory','error');
              return;
          } else if(googlePhone.length!=10){
            popUpMessage('Phone number is invalid','error');
            return;
          }
          $('#loader').attr("hidden",false);
          $(".bgBlur").css("filter", "blur(2px)");
        auth2.signIn().then(() => {
            var cUser = auth2.currentUser.get();
            var profile = cUser.getBasicProfile();
            var uid = profile.getId();
            var token = cUser.getAuthResponse().access_token;
            var idToken =  cUser.getAuthResponse().id_token;
            var provider= 'google';
            var name = profile.getName();
            var email = profile.getEmail();
            var image = profile.getImageUrl();
            var password = Math.floor(Math.random() * 1000000000);
            var objGoogle = {
                "name":name,
                "email":email,
                "login":googlePhone,
                "group":["satcp"],
                "password":password,
                "meta":{    
                    "tocAgree":true,
                    "phone":googlePhone
                    },
                "sso":"google",
                "autologin":true
                }
                let rawdata = JSON.stringify(objGoogle);

                http.post(
                    {
                        url:apiUrl+'auth/register', 
                        data:rawdata, 
                        contentType:'application/json'
                    },
                    (data,textStatus)=>{
                     $('#loader').attr("hidden",true);
                    if(textStatus == '402' || textStatus ==  '403'){
                        //$('p.signupMessage').html(data.responseJSON.message); 
                        $("#sign_up").trigger("reset"); 
                        $(".bgBlur").css("filter", "blur(0px)");
                        popUpMessage(data.message,'warning');

                        return;
                    }
                    loginAuth.setLoginData(data);
                    window.location = satDeliver;
                });
          }).catch((error) => {
            console.error('Google Sign Up or Login Error: ', error)
          });
        });
    });


    window.fbAsyncInit = function() {
        FB.init({
          //appId      : '2733021580281136',
          appId      : 1975907095797359,
          cookie     : true,                     // Enable cookies to allow the server to access the session.
          xfbml      : true,                     // Parse social plugins on this webpage.
          //version    : 'v9.0'                   // Use this Graph API version for this call.
          version    : 'v3.2'
        });
    
    
        FB.getLoginStatus(function(response) {   // Called after the JS SDK has been initialized.
          statusChangeCallback(response);        // Returns the login status.
        });
      };

     //facebook login
   $('#facebook-button').on('click', function() {

    checkLoginState();
    var facebookPhone = $('#phoneFacebook').val();
    if(facebookPhone == ''){
        popUpMessage('Phone number is mandatory','error');
        return;
    }else if(facebookPhone.length!=10){
        popUpMessage('Phone number is invalid','error');
        return;
    }

    $('.fb-login-button').trigger('click');

    function statusChangeCallback(response) {  // Called with the results from FB.getLoginStatus().
        console.log('statusChangeCallback');
        console.log(response);                   // The current login status of the person.
        if(response.status=='connected'){
            testAPI(); 
        }  else {
            FB.login(function(response) {
                 if(response.status=='connected'){
                    testAPI(); 
                }
            });

        }
      }
    
    function checkLoginState() {               // Called when a person is finished with the Login Button.
        FB.getLoginStatus(function(response) {   // See the onlogin handler
          statusChangeCallback(response);
          
        });
      }
    
    function testAPI() {                      // Testing Graph API after login.  See statusChangeCallback() for when this call is made.
        console.log('Welcome!  Fetching your information.... ');
        FB.api('/me?fields=name,email,picture', function(response) {

          console.log('Successful login for: ' + response.name);
           var objFacebook = {
          "name":response.name,
          "email":response.email,
          "login":facebookPhone,
          "group":[SIGNUPGROUP],
          "password":Math.floor(Math.random() * 1000000000),
          "meta":{    
              "tocAgree":true,
              "phone":facebookPhone,
             
              },
          "sso":"facebook",
          "autologin":true
          }

          let rawdata = JSON.stringify(objFacebook);
          $(".bgBlur").css("filter", "blur(2px)");
          http.post(
                    {
                        url:apiUrl+'auth/register', 
                        data:rawdata, 
                        contentType:'application/json'
                    },
                    (data,textStatus)=>{
                    $('#loader').attr("hidden",true);
                    if(textStatus == '402' || textStatus ==  '403'){
                        //$('p.signupMessage').html(data.responseJSON.message);
                        $("#sign_up").trigger("reset"); 
                        $(".bgBlur").css("filter", "blur(0px)");
                        popUpMessage(data.message,'warning');

                        return;
                    }
                    loginAuth.setLoginData(data);
                    window.location = satDeliver;
                });

        });
      }
  });

}