
(function(){
    var http = {
        post: function(postObj, callback){
            $.ajax({
                type: "POST",
                url: postObj.url,
                data: postObj.data,
                contentType: postObj.contentType || 'application/x-www-form-urlencoded; charset=UTF-8',
                success: function(data,textStatus){
                    callback(data,textStatus);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR.status,'Server Error');
                    callback(jqXHR.responseJSON, jqXHR.status);
                }
            });
        },
        get: function(getObj, callback){
            $.ajax({
                type: "GET",
                url: getObj.url,
                data: getObj.data,
                contentType: getObj.data || 'application/x-www-form-urlencoded; charset=UTF-8',
                success: function(data){
                    callback(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    callback(jqXHR.responseJSON, jqXHR.status);
                }
            });
        },
        postAfterLogin: function(postObj, callback){
            $.ajax({
                type: "POST",
                url: postObj.url,
                data: postObj.data,
                contentType: postObj.contentType || 'application/x-www-form-urlencoded; charset=UTF-8',
                beforeSend: function(request) {
                    request.setRequestHeader("authToken", localStorage.getItem('authToken'));
                    request.setRequestHeader("sessionID", localStorage.getItem('sessionID'));
                  },
                success: function(data,textStatus, xhr){
                    console.log('from success==>',textStatus)
                    callback(data,textStatus, xhr);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('from error')
                    callback(jqXHR.responseJSON, jqXHR.status);
                    console.log(jqXHR.status,'Server Error:', errorThrown);
                }
            });
        },
        getafterlogin: function(getObj, callback){
            $.ajax({
                type: "GET",
                url: getObj.url,
                data: getObj.data,
                contentType: getObj.data || 'application/x-www-form-urlencoded; charset=UTF-8',
                beforeSend: function(request) {
                    request.setRequestHeader("authToken", localStorage.getItem('authToken'));
                    request.setRequestHeader("sessionID", localStorage.getItem('sessionID'));
                  },
                success: function(data,textStatus){
                    console.log(textStatus,'Server succcess');
                    callback(data,textStatus);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR.status,'Server Error');
                    callback(jqXHR.responseJSON, jqXHR.status);
                }
            });
        },
        methodAfterlogin: function(getObj, callback){
            $.ajax({
                type: getObj.method,
                url: getObj.url,
                data: getObj.data,
                contentType: getObj.contentType || 'application/x-www-form-urlencoded; charset=UTF-8',
                beforeSend: function(request) {
                    request.setRequestHeader("authToken", localStorage.getItem('authToken'));
                    request.setRequestHeader("sessionID", localStorage.getItem('sessionID'));
                  },
                success: function(data,textStatus){
                    callback(data, textStatus);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR.status,'Server Error');
                    callback(jqXHR.responseJSON, jqXHR.status);
                }
            });
        },

         methodAfterloginPromise: function(getObj){
             return new Promise((resolve, reject)=>{
                $.ajax({
                    type: getObj.method,
                    url: getObj.url,
                    data: getObj.data,
                    contentType: getObj.contentType || 'application/x-www-form-urlencoded; charset=UTF-8',
                    beforeSend: function(request) {
                        request.setRequestHeader("authToken", localStorage.getItem('authToken'));
                        request.setRequestHeader("sessionID", localStorage.getItem('sessionID'));
                    },
                    success: function(data,textStatus){
                        resolve(data, textStatus);
                    }, 
                    error: function (jqXHR, textStatus, errorThrown) {
                         resolve(jqXHR.responseJSON, jqXHR.status);
                    }
                });
            })
        },

        setCookie: function (cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires="+d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
          },
           getCookie: function(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i = 0; i < ca.length; i++) {
              var c = ca[i];
              while (c.charAt(0) == ' ') {
                c = c.substring(1);
              }
              if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
              }
            }
            return "";
          },          
          deleteCookies: function() { 
            var allCookies = document.cookie.split(';');
            var d = new Date();
            d.setTime(d.getTime());
            var expires = "expires="+d.toUTCString();
            for (var i = 0; i < allCookies.length; i++) {
                    var cokie = (allCookies[i]).split('=');
                    document.cookie = cokie[0] + "=;"+expires+";path=/";
                }
            },
        setLocalStroge: function(key,data){
            return localStorage.setItem(key, data);
        },
        removeLocalStroge: function(key){
            return localStorage.removeItem(key);
        },
        getLocalstroage: function(key){
            return localStorage.getItem(key);
        },
        serializeObject(formData) {
        var o = {meta:{}};
        var a = formData;
        $.each(a, function() {            
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
      //  o['groupinfo'] = true;
        return o;
    },
    
    parseURLParams(url) {
        console.log(url);
        var queryStart = url.indexOf("?") + 1,
            queryEnd   = url.indexOf("#") + 1 || url.length + 1,
            query = url.slice(queryStart, queryEnd - 1),
            pairs = query.replace(/\+/g, " ").split("&"),
            parms = {}, i, n, v, nv;
        if (query === url || query === "") return;
        // for (i = 0; i < pairs.length; i++) {
        //     nv = pairs[i].split("=", 2);
        //     n = decodeURIComponent(nv[0]);
        //     v = decodeURIComponent(nv[1]);
    
        //     if (!parms.hasOwnProperty(n)) parms[n] = [];
        //     parms.push(nv.length === 2 ? v : null);
        // }

        for (i = 0; i < pairs.length; i++) {
            nv = pairs[i].split("=", 2);
            n = decodeURIComponent(nv[0]);
            v = decodeURIComponent(nv[1]);
            console.log(nv);
            parms[n]=v;
        }
        return parms;
    }

    };
    window.http = http;
}());
